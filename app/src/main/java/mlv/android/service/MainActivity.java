package mlv.android.service;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import mlv.android.sevices.MyService;

public class MainActivity extends AppCompatActivity {


    private boolean serviceStarted = false;
    private Button startStopServiceButton;
    private Button incrementByOneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startStopServiceButton = (Button) findViewById(R.id.startStopServiceButton);
        incrementByOneButton = (Button) findViewById(R.id.IncrementByOneButton);

        startStopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOrStopService();
            }
        });

        incrementByOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementServiceCounter();
            }
        });
    }

    private void incrementServiceCounter() {
        if (!serviceStarted) {
            Toast.makeText(this, "Service not started", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent i = new Intent(this, MyService.class);
        i.putExtra("VALUE", 1);
        Log.i(MainActivity.class.getName(), "Adding 1");
        startService(i);
        serviceStarted = true;
    }

    private void startOrStopService() {

        if (!serviceStarted) {
            Intent i = new Intent(this, MyService.class);
            Log.i(MainActivity.class.getName(), "Starting service...");
            startService(i);
            serviceStarted = true;
            startStopServiceButton.setText("Stop Service");
        } else {
            Intent i = new Intent(this, MyService.class);
            Log.i(MainActivity.class.getName(), "Stopping service...");
            stopService(i);
            serviceStarted = false;
            startStopServiceButton.setText("Start Service");
        }
    }
}
