package mlv.android.sevices;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import mlv.android.service.R;

/**
 * Created by davide on 09/01/2017.
 */

public class MyService extends Service {
    private static int counter = 0;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SystemClock.sleep(1000);
        MediaPlayer mPlayer = MediaPlayer.create(getBaseContext(), R.raw.coucou);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.start();



        counter += intent.getIntExtra("VALUE", 0);
        Log.i("Service", "Counter: "+counter);
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
